package com.model.service;

import com.controller.CustomException;
import com.model.dao.factory.DaoFactory;
import com.model.dao.factory.TariffDao;
import com.model.entity.Tariff;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class TariffServiceTest {
    @Mock
    private DaoFactory daoFactory;
    @Mock
    private TariffDao tariffDao;

    @InjectMocks
    private TariffService subject;

    @BeforeEach
    void setUp() {
        System.setProperty("java.naming.factory.initial", "com.helper.MyContextFactory");
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllTariffByServiceAndSort() {
        when(daoFactory.createTariffDao()).thenReturn(tariffDao);

        List<Tariff> expect = new ArrayList<>();
        when(tariffDao.findAllTariffFromOneServiceAndSorted(11L, "sortCommand")).thenReturn(expect);

        List<Tariff> actual = subject.getAllTariffByServiceAndSort(11L, "sortCommand");
        assertSame(expect, actual);
    }

    @Test
    void addTariff() throws CustomException {
        when(daoFactory.createTariffDao()).thenReturn(tariffDao);

        Tariff expected = new Tariff("nameTariff", 1000L, BigDecimal.TEN);

        subject.addTariff(1000L, "nameTariff", BigDecimal.TEN);

        verify(tariffDao).create(expected);
    }
}