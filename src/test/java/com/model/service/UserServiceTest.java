package com.model.service;

import com.model.dao.factory.DaoFactory;
import com.model.dao.factory.UserDao;
import com.model.entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.when;

class UserServiceTest {

    @Mock
    private DaoFactory daoFactory;
    @Mock
    private UserDao userDao;

    @InjectMocks
    private UserService subject;

    @BeforeEach
    void setUp() {
        System.setProperty("java.naming.factory.initial", "com.helper.MyContextFactory");
        MockitoAnnotations.initMocks(this);
    }
    @Test
    void getAllUsers() {
        when(daoFactory.createUserDao()).thenReturn(userDao);

        List<User> expect = new ArrayList<>();
        when(userDao.findAll()).thenReturn(expect);

        List<User> actual = subject.getAllUsers();
        assertSame(expect,actual);
    }
}