<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/jspf/taglib.jspf" %>

<html>
<head>
    <title>Internet Provider</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

</head>

<body>
<%@ include file="/WEB-INF/jspf/local.jspf" %>

<h1><fmt:message key="index_jsp.header.home_page"/></h1><br/>
<br/>

<h5>

    <a class="text-decoration-none" href="${pageContext.request.contextPath}/login "><fmt:message key="index_jsp.header.login"/></a>
</h5>

</body>
</html>
