<%@ include file="/WEB-INF/jspf/taglib.jspf" %>
<html>
<head>
    <title>ADMIN</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">


</head>
<body>
<%@ include file="/WEB-INF/jspf/local.jspf" %>
<h1><fmt:message key="adminBasis_jsp.header.main_page"/></h1>

<p> <a class="text-decoration-none" href="${pageContext.request.contextPath}/getAllUser"><fmt:message key="adminBasis_jsp.ref.show_all_user"/></a></p>
<p> <a class="text-decoration-none" href="${pageContext.request.contextPath}/showAllTariff"><fmt:message key="adminBasis_jsp.ref.show_all_tariff"/></a></p>
<%@ include file="/WEB-INF/jspf/logout.jspf" %>
</body>
</html>