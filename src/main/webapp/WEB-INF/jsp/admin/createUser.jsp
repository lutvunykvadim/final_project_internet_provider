<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/jspf/taglib.jspf" %>
<html>
<head>
    <title>Create User</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

</head>
<body>
<%@ include file="/WEB-INF/jspf/local.jspf" %>
<form method="post" action="${pageContext.request.contextPath}/createUser">
    <label> <fmt:message key="login_jsp.label.login"/>
        <input type="text" name="login">
    </label>
    <label> <fmt:message key="login_jsp.label.password"/>
        <input type="password" name="password">
    </label>
    <input class="button" type="submit" value="<fmt:message key="create"/>">
</form>


<p><a class="text-decoration-none" href="${pageContext.request.contextPath}/adminBasis"><fmt:message key="admin_main"/></a></p>
<%@ include file="/WEB-INF/jspf/logout.jspf" %>

</body>
</html>