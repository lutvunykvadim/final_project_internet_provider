package com.controller.command;

import com.controller.constants.Path;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashSet;

public class LogOutCommand implements Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session;


// invalidate user session
        session = request.getSession(false);
        if (session != null)
            session.invalidate();

        return Path.REDIRECT_INDEX_JSP;
    }
}
