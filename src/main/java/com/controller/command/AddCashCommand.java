package com.controller.command;

import com.controller.CustomException;
import com.controller.constants.ExceptionMassage;
import com.controller.constants.Path;
import com.model.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;

public class AddCashCommand implements Command {
    private final UserService service = new UserService();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CustomException {
        BigDecimal inputCash;

//check value is not empty or null
        String inputStringCash = request.getParameter("inputCash");
        if (inputStringCash == null || inputStringCash.isEmpty()) {
            return Path.WEB_INF_USER_ADD_CASH_JSP;
        }

//check value is number
        try {
            inputCash = new BigDecimal(inputStringCash);
        } catch (NumberFormatException e) {
            throw new CustomException(ExceptionMassage.VALUE_ENTERED_MUST_BE_A_NUMBER_GREATER_THAN_0);
        }

//check number value > 0
        if (inputCash.compareTo(BigDecimal.ZERO) <= 0) {
            throw new CustomException(ExceptionMassage.VALUE_MUST_BE_GREATER_THAN_0);
        }
// add value to user
        String login = (String) request.getSession().getAttribute("login");
        service.addCashToUser(login, inputCash);

        return Path.REDIRECT_USER_BASIS;
    }
}
