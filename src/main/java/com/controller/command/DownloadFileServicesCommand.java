package com.controller.command;

import com.controller.constants.Path;
import com.model.service.ServiceService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Properties;

public class DownloadFileServicesCommand implements Command {
    private static final String ATTACHMENT_FILENAME_SERVICES_TXT = "attachment; filename=Services.txt";
    private static final String CONTENT_DISPOSITION = "Content-disposition";
    private static final String TEXT_PLAIN = "text/plain";
    private static final String FILE_NAME = "Services.txt";
    public static final String PATH_FROM_DOWNLOAD_PROPERTIES = "pathFromDownload.properties";
    private final ServiceService service = new ServiceService();

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        resp.setContentType(TEXT_PLAIN);
        resp.setHeader(CONTENT_DISPOSITION, ATTACHMENT_FILENAME_SERVICES_TXT);
        String filePath = "";

// create a new price list
        service.exportServicesToFile();

// read the path to the file
        try (InputStream input = new FileInputStream(PATH_FROM_DOWNLOAD_PROPERTIES)) {

            Properties prop = new Properties();

            // load a properties file
            prop.load(input);
            filePath = prop.getProperty("file_path");
        } catch (IOException e) {
           e.printStackTrace();
       }

        try (FileInputStream inputStream = new FileInputStream(filePath + FILE_NAME);
             OutputStream out = resp.getOutputStream()) {

            int numBytesRead;
            while ((numBytesRead = inputStream.read()) != -1) {
                out.write(numBytesRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Path.REDIRECT_USER_BASIS;
    }
}
