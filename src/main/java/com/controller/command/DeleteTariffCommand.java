package com.controller.command;

import com.controller.CustomException;
import com.controller.constants.Path;
import com.model.service.TariffService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteTariffCommand implements Command {
    private final TariffService service = new TariffService();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CustomException {
        long idTariff = Long.parseLong(request.getParameter("idTariff"));

//delete tariff from all tariffs and all users
        service.deleteTariff(idTariff);
        return Path.REDIRECT_SHOW_ALL_TARIFF;
    }
}
