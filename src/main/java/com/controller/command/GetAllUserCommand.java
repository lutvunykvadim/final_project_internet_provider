package com.controller.command;

import com.controller.constants.Path;
import com.model.entity.User;
import com.model.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GetAllUserCommand implements Command {
    private final UserService service = new UserService();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        List<User> userList;

        // get all users
        userList = service.getAllUsers();

        //put all users in page
        request.setAttribute("userList", userList);

        return Path.WEB_INF_ADMIN_SHOW_ALL_USERS_JSP;
    }
}
