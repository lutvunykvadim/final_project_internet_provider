package com.controller.command;

import com.controller.CustomException;
import com.controller.constants.Path;
import com.model.service.UserOrderBeanService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteTariffForUserCommand implements Command {
    private final UserOrderBeanService beanService = new UserOrderBeanService();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CustomException {
        long idOrder = Long.parseLong(request.getParameter("idOrder"));

//delete tariff from user-order
        beanService.deleteTariffForUser(idOrder);

        return Path.REDIRECT_USER_BASIS;
    }
}
