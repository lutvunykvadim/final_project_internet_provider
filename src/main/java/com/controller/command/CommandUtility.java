package com.controller.command;

import com.model.entity.Role;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashSet;

class CommandUtility {

    //assigning a role to the user
    static void setUserLoginAndRole(HttpServletRequest request,
                                    Role role, String login) {
        HttpSession session = request.getSession();
        session.setAttribute("login", login);
        session.setAttribute("userRole", role);
    }

    // check if the user is already logged in
    static boolean checkUserIsLogged(HttpServletRequest request, String login) {
        @SuppressWarnings("unchecked") HashSet<String> loggedUsers = (HashSet<String>) request.getSession().getServletContext()
                .getAttribute("loggedUsers");

        return loggedUsers.stream().anyMatch(login::equals);
    }
    // adding a login to the HashSet of already logged in
    static void addUserInLogged(HttpServletRequest request, String login) {
        @SuppressWarnings("unchecked") HashSet<String> loggedUsers = (HashSet<String>) request.getSession().getServletContext()
                .getAttribute("loggedUsers");

        loggedUsers.add(login);
        request.getSession().getServletContext()
                .setAttribute("loggedUsers", loggedUsers);
    }
}
