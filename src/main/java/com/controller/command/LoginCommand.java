package com.controller.command;


import com.controller.constants.ExceptionMassage;
import com.controller.constants.Path;
import com.model.entity.Role;
import com.model.entity.Status;
import com.model.entity.User;
import com.model.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginCommand implements Command {
    public static final String ERROR_MESSAGE = "errorMessage";
    private final UserService service = new UserService();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forward = Path.WEB_INF_ERROR_JSP;
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        User user;
        Role userRole;


        if (login == null || password == null) {
            return Path.WEB_INF_LOGIN_JSP;
        }

// check  validation data

        if (login.isEmpty() || password.isEmpty()) {
            request.setAttribute(ERROR_MESSAGE, ExceptionMassage.LOGIN_PASSWORD_CANNOT_BE_EMPTY);

            return forward;
        }

        // check if the user has logged in

        if (CommandUtility.checkUserIsLogged(request, login)) {

            request.setAttribute(ERROR_MESSAGE, ExceptionMassage.USER_IS_ALREADY_LOGGED_IN);

            return forward;
        }

        //check login and password

        if (service.userIsExist(login, password)) {

            user = service.getUser(login);
            userRole = user.getRole();

            // check user lock

            if (user.getStatus() == Status.BLOCKED) {
                request.setAttribute(ERROR_MESSAGE, ExceptionMassage.USER_IS_BLOCKED);
                return forward;
            }

            CommandUtility.addUserInLogged(request, login);

            //transition according to the role
            return moveToMenu(request, login, userRole);
        }

        request.setAttribute(ERROR_MESSAGE, ExceptionMassage.CANNOT_FIND_USER_WITH_SUCH_LOGIN_PASSWORD);

        return forward;
    }

    // transition according to the role
    private String moveToMenu(HttpServletRequest request, String login, Role userRole) {

        if (userRole == Role.ADMIN) {

            CommandUtility.setUserLoginAndRole(request, Role.ADMIN, login);
            return Path.REDIRECT_ADMIN_BASIS;
        }
        if (userRole == Role.USER) {

            CommandUtility.setUserLoginAndRole(request, Role.USER, login);
            return Path.REDIRECT_USER_BASIS;
        }

        return Path.WEB_INF_ERROR_JSP;
    }
}
