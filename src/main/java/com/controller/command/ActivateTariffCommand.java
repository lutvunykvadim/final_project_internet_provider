package com.controller.command;

import com.controller.CustomException;
import com.controller.constants.ExceptionMassage;
import com.controller.constants.Path;
import com.model.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ActivateTariffCommand implements Command {
    private static final Logger log = Logger.getLogger(ActivateTariffCommand.class);
    private final UserService userService = new UserService();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CustomException {
        log.debug("Command starts");
        String login = (String) request.getSession().getAttribute("login");
        long idTariff = Long.parseLong(request.getParameter("idTariff"));

//withdraw cash from the user at the tariff
        if (!userService.withdrawCashFromUser(login, idTariff)) {
            throw new CustomException(ExceptionMassage.NOT_ENOUGH_CASH_PLEASE_TOP_UP_YOUR_CASH_ACCOUNT);
        }
        log.trace("Withdraw cash from the user " + login + " at the tariff " + idTariff);
        log.debug("Command finished");
        return Path.REDIRECT_USER_BASIS;
    }
}
