package com.controller.command;

import com.controller.CustomException;
import com.controller.constants.ExceptionMassage;
import com.controller.constants.Path;
import com.model.service.TariffService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;

public class CreateTariffCommand implements Command {
    private final TariffService service = new TariffService();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CustomException {
        String stringIdService = request.getParameter("id_service");

        if (stringIdService == null) {
            return Path.WEB_INF_ADMIN_CREATE_TARIFF_JSP;
        }
// get value from form
        long idService = Long.parseLong(stringIdService);
        String nameTariff = request.getParameter("nameTariff");
        BigDecimal cost = new BigDecimal(request.getParameter("cost"));


//check number value > 0
        if (cost.compareTo(BigDecimal.ZERO) <= 0) {
            throw new CustomException(ExceptionMassage.VALUE_MUST_BE_GREATER_THAN_0);
        }
// add tariff to all tariffs
        service.addTariff(idService, nameTariff, cost);

        return Path.REDIRECT_SHOW_ALL_TARIFF;

    }
}
