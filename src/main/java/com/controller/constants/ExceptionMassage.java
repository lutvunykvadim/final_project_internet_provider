package com.controller.constants;

public final class ExceptionMassage {
    public static final String NOT_ENOUGH_CASH_PLEASE_TOP_UP_YOUR_CASH_ACCOUNT =
            "Not enough cash, please top up your cash account";
    public static final String VALUE_ENTERED_MUST_BE_A_NUMBER_GREATER_THAN_0 = "Value entered must be a number greater than 0";
    public static final String VALUE_MUST_BE_GREATER_THAN_0 = "Value must be greater than 0";
    public static final String LOGIN_PASSWORD_CANNOT_BE_EMPTY = "Login/password cannot be empty";
    public static final String USER_IS_ALREADY_LOGGED_IN = "User is already logged in";
    public static final String USER_IS_BLOCKED = "User is blocked";
    public static final String CANNOT_FIND_USER_WITH_SUCH_LOGIN_PASSWORD = "Cannot find user with such login/password";
    public static final String TARIFF_FOR_THIS_SERVICE_HAS_ALREADY_BEEN_ORDERED = "Tariff for this service has already been ordered";
    public static final String CANNOT_DELETE_SERVICE = "Cannot delete service ";
    public static final String CANNOT_UPDATE_SERVICE = "Cannot update service ";
    public static final String CANNOT_CREATE_SERVICE = "Cannot create service";
    public static final String CANNOT_CREATE_TARIFF = "Cannot create tariff";
    public static final String CANNOT_UPDATE_TARIFF = "Cannot update tariff ";
    public static final String CANNOT_DELETE_TARIFF = "Cannot delete tariff ";
    public static final String CANNOT_CREATE_USER = "Cannot create user ";
    public static final String CANNOT_DELETE_USER = "Cannot delete user ";
    public static final String CANNOT_UPDATE_USER = "Cannot update user ";
    public static final String CANNOT_CREATE_USER_ORDER = "Cannot create user-order ";
    public static final String CANNOT_DELETE_USER_ORDER = "Cannot delete user-order ";
    public static final String CANNOT_DELETE_USER_ORDER_FROM_ID_TARIFF = "Cannot delete user-order from idTariff ";
    public static final String CANNOT_UPDATE_USER_ORDER = "Cannot update user-order ";
    public static final String YOU_DO_NOT_HAVE_PERMISSION_TO_ACCESS_THE_REQUESTED_RESOURCE = "You do not have permission to access the requested resource";
    public static final String CANNOT_FIND_USER_LOGIN = "Cannot find user login";
    public static final String CANNOT_FIND_USER_ID = "Cannot find user id";
    public static final String CANNOT_FIND_USER_ORDER_BY_ID_TARIFF = "Cannot find userOrder by idTariff ";
    public static final String CANNOT_FIND_USER_ORDER_ID = "Cannot find userOrder id";
    public static final String AND_ID_USER = " and idUser ";
    public static final String CANNOT_FIND_SERVICE_ID = "Cannot find service id";
}
