package com.controller.constants;

public final class Path {
    private Path() {
    }

    // pages
    public static final String WEB_INF_ADMIN_ADMIN_BASIS_JSP = "/WEB-INF/jsp/admin/adminBasis.jsp";
    public static final String WEB_INF_ADMIN_SHOW_ALL_USERS_JSP = "/WEB-INF/jsp/admin/showAllUsers.jsp";
    public static final String WEB_INF_ADMIN_SHOW_ALL_TARIFF_JSP = "/WEB-INF/jsp/admin/showAllTariff.jsp";
    public static final String WEB_INF_ADMIN_SHOW_USER_JSP = "/WEB-INF/jsp/admin/showUser.jsp";
    public static final String WEB_INF_LOGIN_JSP = "/WEB-INF/jsp/login.jsp";
    public static final String WEB_INF_USER_ADD_CASH_JSP = "/WEB-INF/jsp/user/addCash.jsp";
    public static final String WEB_INF_ADMIN_CREATE_TARIFF_JSP = "/WEB-INF/jsp/admin/createTariff.jsp";
    public static final String WEB_INF_USER_SHOW_LIST_TARIFF_JSP = "/WEB-INF/jsp/user/showListTariff.jsp";
    public static final String WEB_INF_USER_USERBASIS_JSP = "/WEB-INF/jsp/user/userBasis.jsp";
    public static final String WEB_INF_USER_SHOW_LIST_SERVICES_JSP = "/WEB-INF/jsp/user/showListServices.jsp";
    public static final String WEB_INF_ADMIN_CREATE_USER_JSP = "/WEB-INF/jsp/admin/createUser.jsp";
    public static final String WEB_INF_ERROR_JSP = "/WEB-INF/jsp/error.jsp";

    //commands
    public static final String REDIRECT_INDEX_JSP = "redirect:/index.jsp";
    public static final String REDIRECT_SHOW_USER = "redirect:/showUser";
    public static final String REDIRECT_SHOW_ALL_TARIFF = "redirect:/showAllTariff";
    public static final String REDIRECT_CREATE_USER = "redirect:/createUser";
    public static final String REDIRECT_ADMIN_BASIS = "redirect:/adminBasis";
    public static final String REDIRECT_USER_BASIS = "redirect:/userBasis";
}
