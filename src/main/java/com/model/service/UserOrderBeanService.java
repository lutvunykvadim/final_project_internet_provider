package com.model.service;

import com.controller.CustomException;
import com.model.bean.UserOrderBean;
import com.model.dao.factory.DaoFactory;
import com.model.dao.factory.UserDao;
import com.model.dao.factory.UserOrderDao;

import java.util.List;

public class UserOrderBeanService {
    DaoFactory daoFactory = DaoFactory.getInstance();

    public List<UserOrderBean> getAllOrdersForUserByLogin(String login) {
        List<UserOrderBean> userOrderBeans;

        try (UserDao userDao = daoFactory.createUserDao();
             UserOrderDao orderDao = daoFactory.createUserOrderDao()) {

            long id = userDao.findByLogin(login).getId();
            userOrderBeans = orderDao.findAllOrdersByIdUser(id);

            return userOrderBeans;
        }
    }

    public void deleteTariffForUser(long idOrder) throws CustomException {
        try (UserOrderDao dao = daoFactory.createUserOrderDao()) {

            dao.delete(idOrder);
        }
    }
}
