package com.model.dao.factory;

import com.model.entity.Service;

public interface ServiceDao extends GenericDao<Service> {
}
