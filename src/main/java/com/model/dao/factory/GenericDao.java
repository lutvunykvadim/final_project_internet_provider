package com.model.dao.factory;

import com.controller.CustomException;

import java.util.List;


public interface GenericDao<T> extends AutoCloseable {
    void create(T entity) throws CustomException;

    T findById(long id);

    List<T> findAll();

    void update(T entity) throws CustomException;

    void delete(long id) throws CustomException;

    void close();

    default void close(AutoCloseable autoCloseable) {
        try {
            if (autoCloseable != null) {
                autoCloseable.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
