package com.model.dao.factory;

import com.model.entity.User;

public interface UserDao extends GenericDao<User> {
    User findByLogin(String login);
}

