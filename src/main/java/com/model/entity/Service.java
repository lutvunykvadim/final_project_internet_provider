package com.model.entity;

import java.util.Objects;

public class Service extends Entity {
    private String nameService;

    public String getNameService() {
        return nameService;
    }

    @Override
    public String toString() {
        return "Service{" +
                "nameService='" + nameService + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Service service = (Service) o;
        return Objects.equals(nameService, service.nameService);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameService);
    }

    public void setNameService(String nameService) {
        this.nameService = nameService;
    }

    public Service(Long id, String nameService) {
        super(id);
        this.nameService = nameService;
    }

    public Service() {
    }
}
